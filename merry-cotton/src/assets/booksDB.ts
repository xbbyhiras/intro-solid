export interface IBook {
  title: string
  author: string
  imgsrc: string
}

export const initialBooks: IBook[] = [
  { title: 'Code Complete', author: 'Steve McConnell', imgsrc: 'https://doodleipsum.com/500x500/hand-drawn?i=b48528b59f8d071fde57e51d2d6c433d' },
  { title: 'The Hobbit', author: 'J.R.R. Tolkien', imgsrc: 'https://doodleipsum.com/500x500/hand-drawn?i=9b11b7fde5591be86d8928901e68355f' },
  { title: 'Living a Feminist Life', author: 'Sarah Ahmed', imgsrc: 'https://doodleipsum.com/500x500/hand-drawn?i=74c909f45ddde824d103db53e2a71e63' }
]
