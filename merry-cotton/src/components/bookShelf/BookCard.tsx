import { Component, children, ParentProps } from 'solid-js'
import type { IBook } from '../../assets/booksDB'
import styles from './BookCard.module.css'

interface IBookCard extends ParentProps<Pick<IBook, 'title'|'author'>> {}

export const BookCard: Component<IBookCard> = ({ title, author, children: child }) => {
  const c = children(() => child)
  return (
    <article class={styles.Card}>
      {c()}
      <h5>{title}</h5>
      <p>{author}</p>
      <a href='#'>Check it</a>
    </article>
  )
}
