import { Accessor, Component, For } from 'solid-js'
import type { IBook } from '../../assets/booksDB'
import { BookCard } from './BookCard'
import styles from './BookList.module.css'

interface IBookList {
  books: Accessor<IBook[]>
}
export const BookList: Component<IBookList> = ({ books }) => {

  return (
    <section class={styles.Main}>
      <For each={books()}>
        {({ title, author, imgsrc: src }) => (
          <BookCard title={title} author={author}>
            <img src={src} alt='Card image cap' />
          </BookCard>
        )}
      </For>
    </section>
  )
}
