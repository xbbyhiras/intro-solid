import { Accessor, Component, createSignal } from 'solid-js'
import { initialBooks } from '../../assets/booksDB'
import { AddBook } from './AddBook'
import { BookList } from './BookList'
import styles from './BookShelf.module.css'

interface IBookShelf {
  name?: string
}

export const BookShelf: Component<IBookShelf> = ({ name = 'Solid' }) => {
  const [books, setBooks] = createSignal(initialBooks)
  const totalBooks: Accessor<number> = () => books().length

  return (
    <section class={styles.Main}>
      <h2>{name}'s Bookshelf - {totalBooks()}</h2>
      <BookList books={books} />
      <AddBook add={setBooks} />
    </section>
  )
}
