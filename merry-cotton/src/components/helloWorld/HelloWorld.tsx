import { Component, children } from 'solid-js'
import styles from './HelloWorld.module.css'

export const HelloWorld: Component<any> = (props) => {
  const c = children(() => props.children)
  return (
    <article class={styles.Card}>
      {c()}
      <h5>Lorem ipsum dolor</h5>
      <p>Quis nostrud nisi ut aliquip ea commodo consequat.</p>
      <a href='#'>Check it</a>
    </article>
  )
}
