import type { Component } from 'solid-js'
import { HelloWorld } from './components/helloWorld/HelloWorld'

import styles from './App.module.css'
import { BookShelf } from './components/bookShelf/BookShelf'

export const App: Component = () => {
  return (
    <main class={styles.Main}>
      <HelloWorld>
        <img src='https://doodleipsum.com/500x500/hand-drawn?i=b48528b59f8d071fde57e51d2d6c433d' alt='Card image cap' />
      </HelloWorld>
      <HelloWorld>
        <img src='https://doodleipsum.com/500x500/hand-drawn?i=9b11b7fde5591be86d8928901e68355f' alt='Card image cap' />
      </HelloWorld>
      <HelloWorld>
        <img class='card-img' src='https://doodleipsum.com/500x500/hand-drawn?i=74c909f45ddde824d103db53e2a71e63' alt='Card image cap' />
      </HelloWorld>
      <BookShelf name='Xav' />
    </main>
  )
}
